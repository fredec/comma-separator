<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link rel="stylesheet" href="css/normalize.min.css">
        <link rel="stylesheet" href="slick/slick.css">
        <link rel="stylesheet" href="slick/slick-theme.css">
        <link rel="stylesheet" href="css/main.css">

        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
    <?php
    if (@$_POST['btnSend'] || @$_POST['btnGerar']){
        if ($_POST['source'] != ""){
            $sourceLines = explode("\n",$_POST['source']);

            $destination = array();
            $textareas = array();

            $sep1 = ";";
            if (@$_POST['separator1'] == 'comma')
                $sep1 = ",";
            else if (@$_POST['separator1'] == 'dot-comma')
                $sep1 = ";";
            else if (@$_POST['separator1'] == 'tab')
                $sep1 = "\t";

            foreach($sourceLines as $line=>$s){
                $exploded = explode($sep1,$s);

                foreach ($exploded as $col=>$value){
                    $destination[$line][$col] = $value;
                    $textareas[$col][] = $value;
                }
            }
        }

        if (@$_POST['btnGerar']){
            foreach ($_POST['chk_destination'] as $idx=>$chk){
                $array[$idx] = explode("\n",$_POST[$chk]);
            }
/*
            echo "<pre>";
            print_r($array);
            echo "</pre>";
*/
            
            $result = array();
           foreach ($array as $idx => $value){

                $idxx_atual = -1;
                foreach ($value as $idxx => $v){
                    $idxx_atual = $idxx;

                    if ($_POST['separator'] == 'comma')
                        $sep = ",";
                    else if ($_POST['separator'] == 'dot-comma')
                        $sep = ";";
                    else if ($_POST['separator'] == 'tab')
                        $sep = "\t";

                    if ($idx > 0)
                        $result[$idxx] .= trim($v).$sep;
                    else
                        $result[$idxx] = trim($v).$sep;
                }
           }

           foreach ($result as $id=>$e){
                $result[$id] = substr($e, 0, -1);
           }

            $result = implode("\n", $result);
           
        }
    }
?>

        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div class="header-container">
            <header class="wrapper clearfix">
                <h1 class="title">Comma Separator</h1>
                <nav>
                    <ul>
                        <li><a href="#">nav ul li a</a></li>
                        <li><a href="#">nav ul li a</a></li>
                        <li><a href="#">nav ul li a</a></li>
                    </ul>
                </nav>
            </header>
        </div>

        <div class="main-container">
            <div class="main wrapper clearfix">

                <article>
                    <form name="frmMagic" method="post" action="">
                        <header>
                            <p>Past your CSV text joined by comma, dot comma, dot, or another symbol and feel the magic! (Oh, such a magic!)</p>
                        </header>
                        <section>
                            <h2>Past the original code here:</h2>
                            <p><textarea name="source"><?=(@$_POST['source']?$_POST['source']:"")?></textarea></p>
                            <select name="separator1">
                                    <option value="comma">,</option>
                                    <option value="dot-comma">;</option>
                                    <option value="tab">TAB</option>
                                </select>
                            <input type="submit" name="btnSend" value="Send" />


                            <?php if (@$textareas || @$_POST['btnGerar']){ ?>

                            <div id="slick">
                            <?php foreach ($textareas as $col=>$ta){
                                $content = implode("\n", $ta);

                                if (trim($content) != ""){
                            ?>
                                <div class="textarea">
                                    <div class="drag">
                                        <?php 
                                            $checked = "";
                                            if (@$_POST['btnGerar']){
                                                foreach(@$_POST['chk_destination'] as $chk){
                                                    if ($chk == 'destination_'.$col){
                                                        $checked = "checked='checked'";
                                                        break;
                                                    }
                                                }
                                            }else{
                                                $checked = "checked='checked'";
                                            }
                                        ?>
                                        <input type="checkbox" <?=$checked?> name="chk_destination[]" id="destination_<?=$col?>" value="destination_<?=$col?>" />
                                    </div>
                                    <textarea name="destination_<?=$col?>"><?=$content?></textarea>
                                </div>
                                <?php } ?>
                            <?php } ?>
                                <div style="clear: both"></div>
                            </div>

                            <div>
                                <input type="submit" name="btnGerar" value="Gerar" />
                                <select name="separator">
                                    <option value="comma">,</option>
                                    <option value="dot-comma">;</option>
                                    <option value="tab">TAB</option>
                                </select>
                                <?php 

                                ?>
                                <textarea name="result"><?=@$result?></textarea>
                            </div>

                            <?php } ?>
                        </section>
                    </form>
                </article>

                <aside>
                    <h3>aside</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sodales urna non odio egestas tempor. Nunc vel vehicula ante. Etiam bibendum iaculis libero, eget molestie nisl pharetra in. In semper consequat est, eu porta velit mollis nec. Curabitur posuere enim eget turpis feugiat tempor. Etiam ullamcorper lorem dapibus velit suscipit ultrices.</p>
                </aside>

            </div> <!-- #main -->
        </div> <!-- #main-container -->

        <div class="footer-container">
            <footer class="wrapper">
                <h3>footer</h3>
            </footer>
        </div>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script src="slick/slick.min.js"></script>

        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
    </body>
</html>
